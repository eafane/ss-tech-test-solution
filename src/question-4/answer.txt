Apparently the function receives an array of query options, everything appears to be fine, 
but the problem is that we apply all these filters after having queried all the payments, 
when Eloquent allows us to apply all these filters in the same query, and this way it will 
return only the payments that interest us, and we save ourselves the cost of filtering the 
entire collection of payments with php code.

In the same way, the relationships defined in the models could have been used to make the 
query, and without the need to do an array_merge or go through arrays.

Another point that could be refactored to make everything go faster. For example, 
the first line of PaymentRepository that queries all the payments could be saved 
if we only queried for the number of payments, since the following validation 
only needs that number.
