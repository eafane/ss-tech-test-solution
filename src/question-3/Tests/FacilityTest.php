<?php

namespace Tests;

use Entities\Facility;
use PHPUnit\Framework\TestCase;

class FacilityTest extends TestCase
{

   
   protected $facility = new Facility;


   /**
    * > The setUp() function is called before each test is run
    */
   protected function setUp(): void
   {
      $this->facility = new Facility;
   }

   /**
    * test toArray method
    */
   public function test_to_array()
   {
      $array = $this->facility->toArray($this->facility);
      $this->assertIsArray(
         $array,
         "assert variable is array or not"
      );
   }


   /**
    * It check if the function returns the county name with the correct suffix for the state
    */
   public function test_get_formatted_country()
   {
      $this->facility->setCounty('Usa');
      $this->facility->setState('');
      $formatted = $this->facility->getFormattedCounty();
      $this->assertEquals($formatted, '');

      $this->facility->setCounty('');
      $this->facility->setState('New York');
      $formatted = $this->facility->getFormattedCounty();
      $this->assertEquals($formatted, '');

      $this->facility->setCounty('USA');
      $this->facility->setState('AK');
      $formatted = $this->facility->getFormattedCounty();
      $this->assertEquals($formatted, 'USA Borough');

      $this->facility->setCounty('USA');
      $this->facility->setState('LA');
      $formatted = $this->facility->getFormattedCounty();
      $this->assertEquals($formatted, 'USA Parish');

      $this->facility->setCounty('USA');
      $this->facility->setState('UTAH');
      $formatted = $this->facility->getFormattedCounty();
      $this->assertEquals($formatted, 'USA Country');
   }

   /**
    * Check ths slus generate correctly
    */
   public function test_get_route_params(){
      $this->facility->setState('New York');
      $this->facility->setCounty('USA');
      $this->facility->setCity('The brooks');
      $this->facility->setLocationCode('JJXX+HR8');

      $routeParams=$this->facility->getRouteParams();

      $this->assertNotContains($routeParams['state'], ' ');
      $this->assertNotContains($routeParams['country'], ' ');
      $this->assertNotContains($routeParams['city'], ' ');
      $this->assertNotContains($routeParams['location_code'], ' ');
      
   }
}
