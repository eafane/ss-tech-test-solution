function reverse(array) {
   var reverse = [];
   for (let index = array.length-1; index >= 0; index--) {
      reverse.push(array[index]);
   }
   return reverse;
}

var array= ['Apple', 'Banana', 'Orange', 'Coconut'];
console.log('source:');
console.log(array);

reverse=reverse(array);

console.log('return:');
console.log(reverse);